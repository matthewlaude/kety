package com.example.kety.view

import android.R
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import com.example.kety.adapters.HomeRecycleAdapter
import com.example.kety.databinding.FragmentHomeBinding
import com.example.kety.viewmodel.HomeViewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel by viewModels<HomeViewModel>()

    private lateinit var toolbar: Toolbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        binding.toolbar.apply {
//            R.attr.windowActionBarOverlay = setsupport
//        }


//        binding.rvCategories.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
//        binding.rvCategories.layoutManager = LinearLayoutManager(context)
//        binding.btnButton.setOnClickListener {}
        binding.rvCategories.adapter = HomeRecycleAdapter().apply {
            homeViewModel.getData()
            homeViewModel.data.observe(viewLifecycleOwner) {
                addHomeItem(it[0])
            }
        }
        binding.rvSkinTypes.adapter = HomeRecycleAdapter().apply {
            homeViewModel.getData()
            homeViewModel.data.observe(viewLifecycleOwner) {
                addHomeItem(it[1])
            }
        }
    }
}