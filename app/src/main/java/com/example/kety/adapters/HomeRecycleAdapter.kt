package com.example.kety.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.kety.databinding.ItemHomeBinding
import com.example.kety.view.HomeFragmentDirections

class HomeRecycleAdapter : RecyclerView.Adapter<HomeRecycleAdapter.HomeViewHolder>() {

    private var data = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val binding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return HomeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val dataItem = data[position]
        holder.loadHome(dataItem)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun addHomeItem(data: List<String>) {
        this.data = data.toMutableList()
        notifyDataSetChanged()
    }

    class HomeViewHolder(
        private val binding: ItemHomeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadHome(dataItem: String) {
            binding.tvHome.text = dataItem
//            binding.ivIcon.text = dataItem

            binding.tvHome.setOnClickListener {
                it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDetailFragment(dataItem))
            }
            binding.ivIcon.setOnClickListener {
                it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDetailFragment(dataItem))
            }
        }
    }
}