package com.example.kety.model

import com.example.kety.model.remote.HomeApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object HomeRepo {

    private val HomeApi = object : HomeApi {
        override suspend fun getData(): List<List<String>> {
            return listOf(
                listOf(
                    "Rank",
                    "Hot",
                    "Loved",
                    "Secrets",
                    "Deepcut",
                    "Bookmarks",
                    "Share",
                    "Surprise",
                    "Genius",
                    "Best"
                ),
                listOf(
                    "Normal",
                    "Dry",
                    "Oily",
                    "Combine",
                    "Cracked",
                    "Smooth",
                    "Sensitive",
                    "Aging",
                    "Red",
                    "Uneven"
                )
            )
        }
    }

    suspend fun getData(): List<List<String>> = withContext(Dispatchers.IO) {
        HomeApi.getData()
    }
}