package com.example.kety.model.remote

interface HomeApi {
    suspend fun getData(): List<List<String>>
}