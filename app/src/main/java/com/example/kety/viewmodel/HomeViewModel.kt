package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kety.model.HomeRepo
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val repo = HomeRepo

    private val _data = MutableLiveData<List<List<String>>>()
    val data : LiveData<List<List<String>>> get() = _data

    fun getData() {
        viewModelScope.launch {
            val itemHome = repo.getData()
            _data.value = itemHome
        }
    }
}